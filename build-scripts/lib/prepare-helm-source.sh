#!/usr/bin/env bash

set -eo pipefail

################################################################################
# Environment Setup
################################################################################
GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

################################################################################
# Main Script
################################################################################
display_task "Preparing data sources for helm"

if [ ! -f "${GL_MP_HELM_PREPARED}" ]; then

    if [ "${GL_MP_BUILD_TYPE}" = "${REPOSITORY_BUILD}" ]; then
        "${GL_MP_CHECKOUT_GL_UPSTREAM}"
    fi

    gitlab_chart_path=$(get_gitlab_helm_chart_path)

    [ -d "${gitlab_chart_path}" ] || display_failure "Could not find ${gitlab_chart_path}"

    pushd "${gitlab_chart_path}" > /dev/null

    display_task "Configure helm for templating"
    if helm init --client-only > /dev/null; then
        display_success "complete"
    else
        display_failure "failed"
    fi

    display_task "Adding GitLab charts helm repository"
    if helm repo add gitlab https://charts.gitlab.io/ > /dev/null; then
        display_success "complete"
    else
        display_failure "failed"
    fi

    display_task "Adding Google Marketplace Integration Tools helm repository"
    if helm repo add gke-marketplace-tools https://cloud-marketplace-tools.storage.googleapis.com/charts > /dev/null; then
        display_success "complete"
    else
        display_failure "failed"
    fi

    display_task "Updating helm repository"
    if helm repo update > /dev/null; then
        display_success "complete"
    else
        display_failure "failed"
    fi

    display_task "Updating helm chart dependencies"
    if helm dep update > /dev/null; then
        display_success "complete"
    else
        display_failure "failed"
    fi

    popd > /dev/null

    touch "${GL_MP_HELM_PREPARED}"
fi
